package com.example.gittest1;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


public class MainActivity extends ActionBarActivity implements OnItemClickListener {
	
	private MyAdapter myAdapter;
	private DrawerLayout drawerLayout;
	private ListView listView;
	private ActionBarDrawerToggle drawerListener;
	public int myNum;
	Handler handler = new Handler();
	public int progress = 0;
	private Context context1;
	private static final String FORMAT = "%02d:%02d:%02d";
	private static final String tag = "Main";
	public MalibuCountDownTimer countDownTimer;
	private int timerHasStarted = 0;
	private Button timerBtn;
	public static TextView timer;
	int seconds , minutes;
	private final long startTime = 3600000;
	private final long interval = 1000;
	
	
	//Async initialization
	private static TextView uvIndexReading;
	ArrayList<Integer> myUVData;
	Timer readDataTimer;
	
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Async 
		TextView uvIndexReading = (TextView)findViewById(R.id.uvIndexReading);
		AsyncTaskRunner ut = new AsyncTaskRunner();
        ut.execute(this.getApplicationContext()); 
		//UI Images
        ImageView personImage = (ImageView)findViewById(R.id.personImage);
		ImageView lotionImage = (ImageView)findViewById(R.id.lotionImage);
		
		
		personImage.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, Protection.class);
				startActivity(intent);
				// TODO Auto-generated method stub
			}
			
		});

		lotionImage.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, Protection.class);
				startActivity(intent);
				// TODO Auto-generated method stub
			}
			
		});
		
		uvIndexReading.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, Index.class);
				startActivity(intent);
				// TODO Auto-generated method stub
			}		
		});
		
		
		
		
		//timer
		timerBtn = (Button) this.findViewById(R.id.timerBtn);
		timerBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (timerHasStarted == 0)
				{
					countDownTimer.start();
					timerHasStarted = 1;
					timerBtn.setText(R.string.lotionApplied);
				}
				else if(timerHasStarted == 1)
				{

					countDownTimer.cancel();
					timerHasStarted = 2;
					timerBtn.setText(R.string.lotionApplied);
				}
				else if(timerHasStarted == 2 ){
					timer.setText(R.string.startTime);
					timerBtn.setText("Apply Sunblock");
					timerHasStarted = 0;
					
				}
				
			}
		});
		
		countDownTimer = new MalibuCountDownTimer(startTime, interval);
		timer = (TextView) this.findViewById(R.id.timer);
		
		// Progress Bar
		Resources res = getResources();
		Drawable drawable = res.getDrawable(R.drawable.background);
		final ProgressBar mProgress = (ProgressBar) findViewById(R.id.progressbar1);
		mProgress.setProgress(0);   // Main Progress
		mProgress.setSecondaryProgress(progress); // Secondary Progress
		mProgress.setMax(100); // Maximum Progress
		mProgress.setProgressDrawable(drawable);
				
		//Progress Bar Animation		
		ObjectAnimator animation = ObjectAnimator.ofInt(mProgress, "progress", progress);
		animation.setDuration(1000); // 1 second
		animation.setInterpolator(new DecelerateInterpolator());
		animation.start();
	

		//set drawer lay out
		drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
		//Initializes the list view
		listView = (ListView) findViewById(R.id.drawerList);
		//sets adapter
		myAdapter = new MyAdapter(this);
		listView.setAdapter(myAdapter);
		//starts the listening of clicks on list
		listView.setOnItemClickListener(this);
	
		drawerListener = new ActionBarDrawerToggle(this, drawerLayout, null, R.drawable.burger, R.string.drawer_open);
		drawerLayout.setDrawerListener(drawerListener);
		
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);	
	}
	

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.v("APP", "onPause is called");
		
		// Close the navigation drawer when user clicks HOME or BACK key
		if(drawerLayout.isDrawerOpen(GravityCompat.START)){
			drawerLayout.closeDrawer(listView);
		}
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.v("APP", "onResume is called");
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.v("APP", "onStop is called");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.v("APP", "onDestroy is called");
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		if(drawerListener.onOptionsItemSelected(item)){
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState){
		super.onPostCreate(savedInstanceState);
		drawerListener.syncState();
		
	}
	// Event handler for clicking on Menu Drawer Menu
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		selectItem(position);
		if(position == 0){
			
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, MainActivity.class);
			startActivity(intent);
		}
		else if(position == 1){
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, Index.class);
			startActivity(intent);
		}
		else if(position == 2){
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, Protection.class);
			startActivity(intent);
		}
		else if(position == 3){
			
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, SkinDamage.class);
			startActivity(intent);
		}
		else if(position == 4){
			
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, Health.class);
			startActivity(intent);
		}
		else if(position == 5){
			
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, Forecast.class);
			startActivity(intent);
		}
		else if(position == 6){
			
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, Settings.class);
			startActivity(intent);
		}
		
		
	
	}
	
	//takes position of clicked item and selects the item,calls for title change
	public void selectItem(int position){
		listView.setItemChecked(position, true);	
		//setTitle(options[position]);
	}
	
	// Count Down Timer Class
	class MalibuCountDownTimer extends CountDownTimer
	{

		public MalibuCountDownTimer(long startTime, long interval){
			super(startTime, interval);
		}

		@Override
		public void onFinish(){			
			timer.setText("Time's up!");	
		}

		@TargetApi(Build.VERSION_CODES.GINGERBREAD)
		@Override
		public void onTick(long millisUntilFinished) {
			final String FORMAT = "%02d:%02d:%02d";

			timer.setText(""+String.format(FORMAT, 
				  TimeUnit.MILLISECONDS.toHours(millisUntilFinished), 
				  TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
	              TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
	              TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
	              TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));              
	  }
	}
	
	//Async Task Used to Update the UI based on Data from Bluetooth Sensor
	private class AsyncTaskRunner extends AsyncTask<Context, String, Void> {
		
	private String resp;
	TextView uvIndexReading = (TextView)findViewById(R.id.uvIndexReading);
	ImageView lotionImage = (ImageView)findViewById(R.id.lotionImage);
	ImageView personImage = (ImageView)findViewById(R.id.personImage);
	Resources res = getResources();
	Drawable drawable = res.getDrawable(R.drawable.background);
	final ProgressBar mProgress = (ProgressBar) findViewById(R.id.progressbar1);
	
	//animation		
	ObjectAnimator animation = ObjectAnimator.ofInt(mProgress, "progress", progress);
	
	@Override
	protected Void doInBackground(Context... params) {
		myUVDataBase data2 = new myUVDataBase(params[0]);
		Log.e("MyApp","Do in background is called");
		while(true){
			uvData data = data2.getuvData();
			if(data != null)
			{
				publishProgress(data.getuvIndex());
				//+" - "+data.getTimeStamp());
			}
			else
			{
				publishProgress("Waiting for Data");
			}
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	@Override
	protected void onProgressUpdate(String...params){
		Log.e("MyApp","onprogress update is called");
		TextView skinSafety = (TextView)findViewById(R.id.exposureText);
		skinSafety.setText("+120 mins until Skin damage"); 
		myNum = Integer.parseInt(params[0]);
		ObjectAnimator animation = ObjectAnimator.ofInt(mProgress, "progress", myNum * 10);
		animation.setDuration(1000); // 1 second
		animation.setInterpolator(new DecelerateInterpolator());
		animation.start();
		Log.e("MyApp", ""+ myNum);
		
		switch(myNum){
			case 0:
				uvIndexReading.setText(params[0]);
				uvIndexReading.setTextColor(Color.parseColor("#06d433"));
				lotionImage.setImageResource(R.drawable.lotionsbase);
				personImage.setImageResource(R.drawable.person1);
				mProgress.setProgress(0);   // Main Progress
				break;
			case 1:
				uvIndexReading.setText(params[0]);
				uvIndexReading.setTextColor(Color.parseColor("#06d433"));
				lotionImage.setImageResource(R.drawable.lotionsbase);
				personImage.setImageResource(R.drawable.person1);
				mProgress.setProgress(10);   // Main Progress
				break;
			
			case 2:
				uvIndexReading.setText(params[0]);
				mProgress.setProgress(20);   // Main Progress
				uvIndexReading.setTextColor(Color.parseColor("#06d433"));
				lotionImage.setImageResource(R.drawable.lotionsbase);
				personImage.setImageResource(R.drawable.person1);
				skinSafety.setText("+100 mins until Skin damage");
				break;
			
			case 3:
				uvIndexReading.setText(params[0]);
				mProgress.setProgress(30);   // Main Progress
				uvIndexReading.setTextColor(Color.parseColor("#06d433"));
				lotionImage.setImageResource(R.drawable.lotionsbase);
				personImage.setImageResource(R.drawable.person1);
				skinSafety.setText("+80 mins until Skin damage");
				break;
		
			case 4:
				uvIndexReading.setText(params[0]);
				mProgress.setProgress(40);   // Main Progress
				uvIndexReading.setTextColor(Color.parseColor("#06d433"));
				lotionImage.setImageResource(R.drawable.lotionsbase);
				personImage.setImageResource(R.drawable.sunglasses);
				skinSafety.setText("+70 mins until Skin damage");
				break;
		
			case 5:
				uvIndexReading.setText(params[0]);
				mProgress.setProgress(50);   // Main Progress
				uvIndexReading.setTextColor(Color.parseColor("#f0ff00"));
				lotionImage.setImageResource(R.drawable.lotionsbase);
				personImage.setImageResource(R.drawable.sunglasses);
				skinSafety.setText("~60 mins until Skin damage");
				break;
		
			case 6:
				uvIndexReading.setText(params[0]);
				mProgress.setProgress(60);   // Main Progress
				uvIndexReading.setTextColor(Color.parseColor("#ffb400"));
				lotionImage.setImageResource(R.drawable.lotionsbase);
				personImage.setImageResource(R.drawable.hatglasses1);
				skinSafety.setText("~45 mins until Skin damage");
				break;
		
			case 7:
				MainActivity.uvIndexReading.setText(params[0]);
				mProgress.setProgress(70);   // Main Progress
				uvIndexReading.setTextColor(Color.parseColor("#ffb400"));
				lotionImage.setImageResource(R.drawable.lotionspf30);
				personImage.setImageResource(R.drawable.hatglasses1);
				skinSafety.setText("~30 mins until Skin damage");
				break;
		
			case 8:
				uvIndexReading.setText(params[0]);
				mProgress.setProgress(80);   // Main Progress
				uvIndexReading.setTextColor(Color.parseColor("#ff0000"));
				lotionImage.setImageResource(R.drawable.lotionspf30);
				personImage.setImageResource(R.drawable.hatglasses1);
				skinSafety.setText("~30 mins until Skin damage");
				break;
		
			case 9:
				uvIndexReading.setText(params[0]);
				mProgress.setProgress(90);   // Main Progress
				uvIndexReading.setTextColor(Color.parseColor("#ff0000"));
				lotionImage.setImageResource(R.drawable.lotionspf50);
				personImage.setImageResource(R.drawable.hatglasses1);
				skinSafety.setText("~20 mins until Skin damage");
				break;
		
			case 10:
				uvIndexReading.setText(params[0]);
				mProgress.setProgress(100);   // Main Progress
				uvIndexReading.setTextColor(Color.parseColor("#5200A3"));
				lotionImage.setImageResource(R.drawable.lotionspf50);
				personImage.setImageResource(R.drawable.hatglasses1);
				skinSafety.setText("~20 mins until Skin damage");
				break;
			default:
				uvIndexReading.setText("7");
				uvIndexReading.setTextColor(Color.parseColor("#ff8100"));
				break;
	  }
		
	}
	

}
// Adapter
 static class MyAdapter extends BaseAdapter{
	private Context context;
	String[] options;
	
	int [] images = {R.drawable.home,R.drawable.ic_sun2, R.drawable.timer, R.drawable.umbrella, R.drawable.heart,
			R.drawable.cloud, R.drawable.setting};
	
	
	
	public MyAdapter(Context context) {
		Log.v("myApp","MyAdapter");
		this.context = context;
		options = context.getResources().getStringArray(R.array.options);
		// TODO Auto-generated constructor stub
	}
	@Override
	public int getCount() {
		Log.v("myApp","getCount");
		// TODO Auto-generated method stub
		
		return options.length;
	}

	@Override
	public Object getItem(int position) {
		Log.v("myApp","getItem");
		// TODO Auto-generated method stub
		return options[position];
	}
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.v("myApp","getView");
		// TODO Auto-generated method stub
		View row = null;
		if(convertView == null)
		{
			Log.v("myApp","convertView = null");
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row =inflater.inflate(R.layout.custom_layout, parent, false);
		}
		else
		{
			Log.v("myApp","convertView != null");
			row = convertView;
		}
		Log.v("myApp","Pre return View");
		TextView titleTextView = (TextView)row.findViewById(R.id.textView1);
		ImageView titleImageView = (ImageView)row.findViewById(R.id.imageView1);
		
		titleTextView.setText(options[position]);
		titleImageView.setImageResource(images[position]);
		
		return row;
	}
	
}
}
