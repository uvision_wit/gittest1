package com.example.gittest1;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class myUVDataBase extends SQLiteOpenHelper {
	
	//Datbase version
	private static final int DATABASE_VERSION = 7;
	private static final String DATABASE_NAME = "myDataBase";
	private static final String TABLE_DATA = "myUVData";
	private static final String KEY_ID = "id";
	private static final String KEY_TIME = "time";
	private static final String KEY_UVDATA = "uvData";
	
	private static final String[] COLUMNS = {KEY_ID,KEY_TIME,KEY_UVDATA};
	
	
	
	public myUVDataBase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_MY_TABLE = "CREATE TABLE " + TABLE_DATA + " ( " + 
		"id INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_TIME + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + 
		KEY_UVDATA + " TEXT)";
		db.execSQL(CREATE_MY_TABLE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_DATA);
        
        // create fresh books table
        this.onCreate(db);

	}
	
	public uvData getuvData(){
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = 
                db.query(TABLE_DATA, // a. table
                COLUMNS, // b. column names
                null, // c. selections 
                null, // d. selections args
                null, // e. group by
                null, // f. having
                KEY_TIME + " DESC", // g. order by
                "1"); // h. limit
		
		if(cursor != null && cursor.getCount() > 0){
			cursor.moveToFirst();
		}
		else
		{
			cursor.close();
			return null;
		}
		
		uvData data = new uvData();
		data.setId(Integer.parseInt(cursor.getString(0)));
		data.setTimeStamp(cursor.getString(1));
		data.setuvIndex(cursor.getString(2));
		
		Log.d("getData("+data.getId()+")", data.toString());
		cursor.close();
		return data;
	}

    public void addData(uvData data){
        Log.d("addBook", data.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_UVDATA, data.getuvIndex() ); // get alst name
 
        // 3. insert
        db.insert(TABLE_DATA, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
}