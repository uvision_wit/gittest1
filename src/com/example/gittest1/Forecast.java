package com.example.gittest1;

import com.example.gittest1.MainActivity.MyAdapter;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class Forecast extends ActionBarActivity implements OnItemClickListener {

	private MyAdapter myAdapter;
	private DrawerLayout drawerLayout;
	private ListView listView;
	private ActionBarDrawerToggle drawerListener;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forecast);
		//set drawer lay out
		drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
		//Initializes the list view
		listView = (ListView) findViewById(R.id.drawerList);
		//sets adapter
		myAdapter = new MyAdapter(this);
		listView.setAdapter(myAdapter);
		//starts the listening of clicks on list
		listView.setOnItemClickListener(this);
		
		drawerListener = new ActionBarDrawerToggle(this, drawerLayout, null, R.drawable.burger, R.string.drawer_open);
		drawerLayout.setDrawerListener(drawerListener);
		
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		WebView webview = (WebView)findViewById(R.id.webView);
		webview.setWebViewClient(new WebViewClient());
		
		//Enable JavaScript
		webview.getSettings().setJavaScriptEnabled(true);
		
		//Zoom out of the page to fit the display
		webview.getSettings().setLoadWithOverviewMode(true);
		webview.getSettings().setUseWideViewPort(true);
		
		//Provide pinch zoom operation
		webview.getSettings().setBuiltInZoomControls(true);
		
		webview.loadUrl("http://accuweather.com");
		}			
	
		@Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			Log.v("APP", "onPause is called");
			
			// Close the navigation drawer when user clicks HOME or BACK key
			if(drawerLayout.isDrawerOpen(GravityCompat.START)){
				drawerLayout.closeDrawer(listView);
			}
			
		}
		
		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			Log.v("APP", "onResume is called");
		}
	
		@Override
		protected void onStop() {
			// TODO Auto-generated method stub
			super.onStop();
			Log.v("APP", "onStop is called");
		}
	
		@Override
		protected void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
			Log.v("APP", "onDestroy is called");
		}
	
		@Override
		public boolean onOptionsItemSelected(MenuItem item){
			if(drawerListener.onOptionsItemSelected(item)){
				return true;
			}
			return super.onOptionsItemSelected(item);
		}

		@Override
		protected void onPostCreate(Bundle savedInstanceState){
			super.onPostCreate(savedInstanceState);
			drawerListener.syncState();
				
		}	
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		selectItem(position);
		if(position == 0){
			Intent intent = new Intent();
			intent.setClass(Forecast.this, MainActivity.class);
			startActivity(intent);
		}
		else if(position == 1){
			Intent intent = new Intent();
			intent.setClass(Forecast.this, Index.class);
			startActivity(intent);
		}
		else if(position == 2){
			Intent intent = new Intent();
			intent.setClass(Forecast.this, Protection.class);
			startActivity(intent);
		}
		else if(position == 3){
			Intent intent = new Intent();
			intent.setClass(Forecast.this, SkinDamage.class);
			startActivity(intent);
		}
		else if(position == 4){
			Intent intent = new Intent();
			intent.setClass(Forecast.this, Health.class);
			startActivity(intent);
		}
		else if(position == 5){
			Intent intent = new Intent();
			intent.setClass(Forecast.this, Forecast.class);
			startActivity(intent);
		}
		else if(position == 6){
			Intent intent = new Intent();
			intent.setClass(Forecast.this, Settings.class);
			startActivity(intent);
		}
		finish();
	}
	
	//takes position of clicked item and selects the item,calls for title change
	public void selectItem(int position){
		listView.setItemChecked(position, true);	
		//setTitle(options[position]);
	}
	
	//changes the title of the action bar to the selected option
	public void setTitle(String title)
	{
		getSupportActionBar().setTitle(title);
	}	
}
