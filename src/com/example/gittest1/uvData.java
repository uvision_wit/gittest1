package com.example.gittest1;

public class uvData {
	
	private int id;
	private String timeStamp;
	private String uvIndex;
	
public uvData(){}
	
	public uvData(String timeStamp, String lastuvIndex){
		super();
		this.timeStamp = timeStamp;
		this.uvIndex = uvIndex ;	
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTimeStamp(){
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp){
		this.timeStamp = timeStamp;
	}
	public String getuvIndex(){
		return uvIndex;
	}
	public void setuvIndex(String uvIndex){
		this.uvIndex = uvIndex;
	}
	public String toString() {
		return "Data [id=" + id + ", The UV Index is " + uvIndex  + " at " + timeStamp + " ]";
	}
}
