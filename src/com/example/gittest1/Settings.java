package com.example.gittest1;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

import com.example.gittest1.MainActivity.MyAdapter;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class Settings extends ActionBarActivity implements OnItemClickListener {

	private MyAdapter myAdapter;
	private DrawerLayout drawerLayout;
	private ListView listView;
	private ActionBarDrawerToggle drawerListener;
	
	//bluetooth
	ListView listViewPaired;
    ListView listViewDetected;
    ArrayList<String> arrayListpaired;
    Button buttonSearch,buttonOn,buttonDesc,buttonOff;
    ArrayAdapter<String> adapter,detectedAdapter;
    static HandleSeacrh handleSeacrh;
    BluetoothDevice bdDevice;
    BluetoothClass bdClass;
    ArrayList<BluetoothDevice> arrayListPairedBluetoothDevices;
    private ButtonClicked clicked;
    ListItemClickedonPaired listItemClickedonPaired;
    BluetoothAdapter bluetoothAdapter = null;
    ArrayList<BluetoothDevice> arrayListBluetoothDevices = null;
    ListItemClicked listItemClicked;
    public static TextView uvIndex;
    
    private Bluetooth bt;
    
    private String TAG = "MyApp";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		//DRAWER
		//set drawer lay out
		drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
		//Initializes the list view
		listView = (ListView) findViewById(R.id.drawerList);
		//sets adapter
		myAdapter = new MyAdapter(this);
		listView.setAdapter(myAdapter);
		//starts the listening of clicks on list
		listView.setOnItemClickListener(this);
		
		drawerListener = new ActionBarDrawerToggle(this, drawerLayout, null, R.drawable.burger, R.string.drawer_open);
		drawerLayout.setDrawerListener(drawerListener);
		
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		//DRAWER
		/*
	 	uvIndex = (TextView)findViewById(R.id.uvIndex);
        updateText ut = new updateText();
        ut.execute(this.getApplicationContext());
        */
        listViewDetected = (ListView) findViewById(R.id.listViewDetected);
        listViewPaired = (ListView) findViewById(R.id.listViewPaired);
        
        buttonSearch = (Button) findViewById(R.id.buttonSearch);
        buttonOn = (Button) findViewById(R.id.buttonOn);
        buttonDesc = (Button) findViewById(R.id.buttonDesc);
        buttonOff = (Button) findViewById(R.id.buttonOff); 
        
        arrayListpaired = new ArrayList<String>();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        clicked = new ButtonClicked();
        handleSeacrh = new HandleSeacrh();
        arrayListPairedBluetoothDevices = new ArrayList<BluetoothDevice>();
        
        /*
         * the above declaration is just for getting the paired bluetooth devices;
         * this helps in the removing the bond between paired devices.
         */
        listItemClickedonPaired = new ListItemClickedonPaired();
        arrayListBluetoothDevices = new ArrayList<BluetoothDevice>();
        adapter= new ArrayAdapter<String>(Settings.this, android.R.layout.simple_list_item_1, arrayListpaired);
        detectedAdapter = new ArrayAdapter<String>(Settings.this, android.R.layout.simple_list_item_single_choice);
        listViewDetected.setAdapter(detectedAdapter);
        listItemClicked = new ListItemClicked();
        detectedAdapter.notifyDataSetChanged();
        listViewPaired.setAdapter(adapter);
        
        bt = new Bluetooth(this, mHandler);
	}
	   @Override
	    protected void onStart() {
	        // TODO Auto-generated method stub
	        super.onStart();
	        getPairedDevices();
	        buttonOn.setOnClickListener(clicked);
	        buttonSearch.setOnClickListener(clicked);
	        buttonDesc.setOnClickListener(clicked);
	        buttonOff.setOnClickListener(clicked);
	        listViewDetected.setOnItemClickListener(listItemClicked);
	        listViewPaired.setOnItemClickListener(listItemClickedonPaired);
	    }
	    private void getPairedDevices() {
	        Set<BluetoothDevice> pairedDevice = bluetoothAdapter.getBondedDevices();            
	        if(pairedDevice.size()>0)
	        {
	            for(BluetoothDevice device : pairedDevice)
	            {
	                arrayListpaired.add(device.getName()+"\n"+device.getAddress());
	                arrayListPairedBluetoothDevices.add(device);
	            }
	        }
	        adapter.notifyDataSetChanged();
	    }
	    private Boolean connect(BluetoothDevice bdDevice) { 
	    	
	    	Log.v(TAG, "connect() is called");
	    	
	        Boolean bool = false;
	        try {
	            Log.i("Log", "service method is called ");
	            Class cl = Class.forName("android.bluetooth.BluetoothDevice");
	            Class[] par = {};
	            Method method = cl.getMethod("createBond", par);
	            Object[] args = {};
	            bool = (Boolean) method.invoke(bdDevice);//, args);// this invoke creates the detected devices paired.
	           
	        } catch (Exception e) {
	            Log.i("Log", "Inside catch of serviceFromDevice Method");
	            e.printStackTrace();
	        }
	        return bool.booleanValue();
	    };


	    public boolean removeBond(BluetoothDevice btDevice) throws Exception {  
	    	Log.v(TAG, "removeBond() is called");
	    	
	        Class btClass = Class.forName("android.bluetooth.BluetoothDevice");
	        Method removeBondMethod = btClass.getMethod("removeBond");  
	        Boolean returnValue = (Boolean) removeBondMethod.invoke(btDevice);  
	        return returnValue.booleanValue();  
	    }


	    public boolean createBond(BluetoothDevice btDevice) throws Exception { 
	    	Log.v(TAG, "createBond() is called");
	    	
	        Class class1 = Class.forName("android.bluetooth.BluetoothDevice");
	        Method createBondMethod = class1.getMethod("createBond");  
	        Boolean returnValue = (Boolean) createBondMethod.invoke(btDevice);  
	        return returnValue.booleanValue();  
	    }  
	    
	    

	    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
	    
	    	@Override
	        public void onReceive(Context context, Intent intent) {
	    		
	    		Log.v(TAG, "myReceiver()->onReceive() is called");
	    		
	            Message msg = Message.obtain();
	            String action = intent.getAction();
	            if(BluetoothDevice.ACTION_FOUND.equals(action)){
	                Toast.makeText(context, "ACTION_FOUND", Toast.LENGTH_SHORT).show();

	                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	                try
	                {
	                    //device.getClass().getMethod("setPairingConfirmation", boolean.class).invoke(device, true);
	                    //device.getClass().getMethod("cancelPairingUserInput", boolean.class).invoke(device);
	                }
	                catch (Exception e) {
	                    Log.i("Log", "Inside the exception: ");
	                    e.printStackTrace();
	                }

	                if(arrayListBluetoothDevices.size()<1) // this checks if the size of bluetooth device is 0,then add the
	                {                                           // device to the arraylist.
	                    detectedAdapter.add(device.getName()+"\n"+device.getAddress());
	                    arrayListBluetoothDevices.add(device);
	                    detectedAdapter.notifyDataSetChanged();
	                }
	                else
	                {
	                    boolean flag = true;    // flag to indicate that particular device is already in the arlist or not
	                    for(int i = 0; i<arrayListBluetoothDevices.size();i++)
	                    {
	                        if(device.getAddress().equals(arrayListBluetoothDevices.get(i).getAddress()))
	                        {
	                            flag = false;
	                        }
	                    }
	                    if(flag == true)
	                    {
	                        detectedAdapter.add(device.getName()+"\n"+device.getAddress()+"*********");
	                        arrayListBluetoothDevices.add(device);
	                        detectedAdapter.notifyDataSetChanged();
	                    }
	                }
	            }           
	        }
	    };
	    
	    private void startSearching() {
	    	
	    	Log.v(TAG, "startSearching() is called");
	    	
	    	Log.i("Log", "in the start searching method");
	    	
	        IntentFilter intentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
	        Settings.this.registerReceiver(myReceiver, intentFilter);
	        bluetoothAdapter.startDiscovery();
	    }
	    
	    private void onBluetooth() {
	        if(!bluetoothAdapter.isEnabled())
	        {
	            bluetoothAdapter.enable();
	            
	            Log.v(TAG, "onBluetooth() is called. Bluetooth is enabled");
	            
	            Log.i("Log", "Bluetooth is Enabled");
	        }
	    }
	    
	    private void offBluetooth() {
	        if(bluetoothAdapter.isEnabled())
	        {
	            bluetoothAdapter.disable();
	            Log.v(TAG, "Bluetooth is disabled");
	        }
	    }
	    
	    
	    private void makeDiscoverable() {
	    	
	    	Log.v(TAG, "makeDiscoverable() is called");
	    	
	        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
	        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
	        startActivity(discoverableIntent);
	        
	        Log.i("Log", "Discoverable ");
	    }
	    
	    class ListItemClicked implements OnItemClickListener
	    {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	            // TODO Auto-generated method stub
	            bdDevice = arrayListBluetoothDevices.get(position);
	            
	            Log.i("Log", "The dvice : "+bdDevice.toString());
	            /*
	             * here below we can do pairing without calling the callthread(), we can directly call the
	             * connect(). but for the safer side we must usethe threading object.
	             */        
	            bt.connectDevice(bdDevice);
	        }       
	    }
	    
	    class ListItemClickedonPaired implements OnItemClickListener{
	        @Override
	        public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
	            bdDevice = arrayListPairedBluetoothDevices.get(position);
	            try {
	            	bt.connectDevice(bdDevice);


	               
	            } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	        }
	    }


	    
	    class ButtonClicked implements OnClickListener{
	    	
	        @Override
	        public void onClick(View view) {
	            switch (view.getId()) {
	            case R.id.buttonOn:
	                onBluetooth();
	                break;
	            case R.id.buttonSearch:
	                arrayListBluetoothDevices.clear();
	                startSearching();
	                break;
	            case R.id.buttonDesc:
	                makeDiscoverable();
	                break;
	            case R.id.buttonOff:
	                offBluetooth();
	                break;
	            default:
	                break;
	            }
	        }
	    }

	    private final Handler mHandler = new Handler() {
	        @Override
	        public void handleMessage(Message msg) {
	            switch (msg.what) {
	                case Bluetooth.MESSAGE_STATE_CHANGE:
	                    Log.d(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
	                    break;
	                case Bluetooth.MESSAGE_WRITE:
	                    Log.d(TAG, "MESSAGE_WRITE ");
	                    break;
	                case Bluetooth.MESSAGE_READ:
	                    Log.d(TAG, "MESSAGE_READ ");
	                    break;
	                case Bluetooth.MESSAGE_DEVICE_NAME:
	                    Log.d(TAG, "MESSAGE_DEVICE_NAME "+msg);
	                    break;
	                case Bluetooth.MESSAGE_TOAST:
	                    Log.d(TAG, "MESSAGE_TOAST "+msg);
	                    break;
	            }
	        }
	    };
	    
	    class HandleSeacrh extends Handler
	    {
	        @Override
	        public void handleMessage(Message msg) {
	            switch (msg.what) {
	            case 111:

	                break;

	            default:
	                break;
	            }
	        }
	    }
	    /*
	    //COPY TO MAIN ACTIVITY IN APP
	    class updateText extends AsyncTask<Context, String, Void>{
	    	
			@Override
			protected Void doInBackground(Context... params) {
				myUVDataBase data2 = new myUVDataBase(params[0]);
				Log.e(TAG,"Do in background is called");
				while(true){
					uvData data = data2.getuvData();
					if(data != null)
					{
						publishProgress(data.getuvIndex());
						//+" - "+data.getTimeStamp());
					}
					else
					{
						publishProgress("Waiting for Data");
					}
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				
				// TODO Auto-generated method stub
				
			}
			@Override
			protected void onProgressUpdate(String...params){
				Log.e(TAG,"onprogress update is called");
				Settings.uvIndex.setText(params[0]);
				
			}
	    	
	    }
		*/
		@Override
		public boolean onOptionsItemSelected(MenuItem item){
			if(drawerListener.onOptionsItemSelected(item)){
				return true;
			}
			return super.onOptionsItemSelected(item);
		}

		@Override
		protected void onPostCreate(Bundle savedInstanceState){
			super.onPostCreate(savedInstanceState);
			drawerListener.syncState();
				
		}	
	//Adapter on click Events in navigation drawer
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		selectItem(position);
		if(position == 0){
			Intent intent = new Intent();
			intent.setClass(Settings.this, MainActivity.class);
			startActivity(intent);
		}
		else if(position == 1){
			Intent intent = new Intent();
			intent.setClass(Settings.this, Index.class);
			startActivity(intent);
		}
		else if(position == 2){
			Intent intent = new Intent();
			intent.setClass(Settings.this, Protection.class);
			startActivity(intent);
		}
		else if(position == 3){
			Intent intent = new Intent();
			intent.setClass(Settings.this, SkinDamage.class);
			startActivity(intent);
		}
		else if(position == 4){
			Intent intent = new Intent();
			intent.setClass(Settings.this, Health.class);
			startActivity(intent);
		}
		else if(position == 5){
			Intent intent = new Intent();
			intent.setClass(Settings.this, Forecast.class);
			startActivity(intent);
		}
		else if(position == 6){
			Intent intent = new Intent();
			intent.setClass(Settings.this, Settings.class);
			startActivity(intent);
		}
		finish();
	}
	
	//takes position of clicked item and selects the item,calls for title change
	public void selectItem(int position){
		listView.setItemChecked(position, true);	
		//setTitle(options[position]);
	}
	
	//changes the title of the action bar to the selected option
	public void setTitle(String title)
	{
		getSupportActionBar().setTitle(title);
	}	
}